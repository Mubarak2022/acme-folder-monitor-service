package com.acme.foldermonitorservice.config;

import com.acme.foldermonitorservice.service.DocChangeListener;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.devtools.filewatch.FileSystemWatcher;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PreDestroy;
import java.io.File;
import java.time.Duration;

@Configuration
@Slf4j
public class DocWatcherConfig {
    @Value("${file.base.path}")
    private String fileBasePath;

    @Bean
    public FileSystemWatcher fileSystemWatcher() {

        FileSystemWatcher fileSystemWatcher = new FileSystemWatcher(true, Duration.ofMillis(500L), Duration.ofMillis(300L));
        fileSystemWatcher.addSourceDirectory(new File(fileBasePath));
        fileSystemWatcher.addListener(new DocChangeListener());
        fileSystemWatcher.start();
        log.info("File Watcher Started ");
        return fileSystemWatcher;
    }

    @PreDestroy
    public void onDestroy() {
        fileSystemWatcher().stop();
    }
}
