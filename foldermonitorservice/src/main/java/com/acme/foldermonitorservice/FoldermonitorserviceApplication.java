package com.acme.foldermonitorservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class FoldermonitorserviceApplication {

    public static void main(String[] args) {
        SpringApplication.run(FoldermonitorserviceApplication.class, args);
    }

}
