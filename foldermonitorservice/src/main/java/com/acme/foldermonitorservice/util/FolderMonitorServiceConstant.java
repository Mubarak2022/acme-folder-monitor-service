package com.acme.foldermonitorservice.util;

import java.io.Serializable;

public final class FolderMonitorServiceConstant implements Serializable {

    public static final String INVALID_FILE_DELETED_FROM = "Invalid file Deleted from : ";

}
