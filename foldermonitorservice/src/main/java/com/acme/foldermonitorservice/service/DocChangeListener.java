package com.acme.foldermonitorservice.service;

import com.acme.foldermonitorservice.exception.ResourceNotFoundException;
import com.acme.foldermonitorservice.util.FolderMonitorServiceConstant;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.boot.devtools.filewatch.ChangedFile;
import org.springframework.boot.devtools.filewatch.ChangedFiles;
import org.springframework.boot.devtools.filewatch.FileChangeListener;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Set;

@Slf4j
@Service
public class DocChangeListener implements FileChangeListener {

    @Override
    public void onChange(Set<ChangedFiles> changeSet) {

        for (ChangedFiles changedFiles : changeSet) {
            for (ChangedFile changedFile : changedFiles.getFiles()) {

                log.info(changedFile.getType() + " : " + changedFile.getFile().getName());
                try {
                    deleteNonNumericDocs(changedFile);
                } catch (IOException e) {
                    throw new ResourceNotFoundException(e.getLocalizedMessage());
                }
            }
        }
    }


    private void deleteNonNumericDocs(ChangedFile changedFile) throws IOException {
        String fName = changedFile.getFile().getName();

        if (!StringUtils.isNumeric(fName)) {
            String filePath = changedFile.getFile().getPath();
            Files.deleteIfExists(new File(changedFile.getFile().getPath()).toPath());
            log.info(FolderMonitorServiceConstant.INVALID_FILE_DELETED_FROM + filePath);
        }


    }

}
