package com.acme.foldermonitorservice.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@Slf4j
public class RestResponseExceptionHandler {


    @ExceptionHandler
    public void handleResourceNotFoundException(ResourceNotFoundException resourceNotFoundException) {
        log.info("ResourceNotFoundException : {}", resourceNotFoundException);
    }

    @ExceptionHandler({Exception.class})
    public void handleGenericException(Exception exception) {
        log.info("GenericException : {}", exception);
    }

}
